//
// Tarot: Tarot Deck Lookup
// src/lib.rs: Library definitions
//
// Copyright (c) 2021 Ali Polatel <alip@exherbo.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

pub mod tarot;
