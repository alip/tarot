//
// Tarot: Tarot Deck Lookup
// src/main.rs: Main entry point
//
// Copyright (c) 2021 Ali Polatel <alip@exherbo.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use log::info;

use rouille::router;

use tarot::tarot;

/*
use log::{Level, LevelFilter, Metadata, Record};
static CONSOLE_LOGGER: ConsoleLogger = ConsoleLogger;
struct ConsoleLogger;

impl log::Log for ConsoleLogger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= Level::Debug
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            println!("{} - {}", record.level(), record.args());
        }
    }

    fn flush(&self) {}
}
*/

fn main() {
    /*
    log::set_logger(&CONSOLE_LOGGER)
        .unwrap_or_else(|e| { panic!("Error setting up logger: {}", e) });
    log::set_max_level(LevelFilter::Info);
    */
    syslog::init(syslog::Facility::LOG_DAEMON,
        log::LevelFilter::Debug,
        Some("tarot"))
        .unwrap_or_else(|e| { panic!("Error setting up logger: {}", e) });

    /*
    info!("Dropping privileges and chrooting to /var/empty");
    privdrop::PrivDrop::default()
        .chroot("/var/empty")
        .user("nobody")
        .apply()
        .unwrap_or_else(|e| {
            warn!("Failed to drop privileges: {}", e);
            panic!("Failed to drop privileges: {}", e)
        });
    */

    info!("Starting server on localhost:8000");
    rouille::start_server("localhost:8000", move |request| {
        router!(request,
            (GET) (/tarot) => {
                let msg = tarot::draw();
                info!("TAROT: {}", msg);
                rouille::Response::text(format!("{}\n", msg))
            },
            _ => rouille::Response::empty_404()
        )
    })
}
